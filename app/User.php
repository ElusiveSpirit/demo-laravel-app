<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class User extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'password_digest', 'firstname', 'lastname', 'birthdate', 'is_active', 'doc_image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password_digest',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
        'birthdate' => 'date',
    ];

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute() {
        return "{$this->firstname} {$this->lastname}";
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getDocImageUrlAttribute() {
//        return Storage::temporaryUrl(
//            $this->doc_image, now()->addDays(5)
//        );
//        return Storage::url($this->doc_image);
        return "/storage/{$this->doc_image}";
    }

    /**
     * Set the user's password.
     *
     * @param  string $value
     * @return void
     */
    public function setPasswordDigestAttribute($value)
    {
        $this->attributes['password_digest'] = Hash::make($value);
    }

    /**
     * Set the user's doc image.
     *
     * @param  UploadedFile $value
     * @return void
     */
    public function setDocImageAttribute($value)
    {
        if (!is_string($value)) {
            $name = time() . $value->getClientOriginalName();
            $filePath = 'images/' . $name;
            Storage::disk('s3')->put($filePath, file_get_contents($value));
            $this->attributes['doc_image'] = $filePath;
        } else {
            $this->attributes['doc_image'] = $value;
        }
    }

    /**
     * Deletes user
     *
     * @return void
     */
    public function softDelete()
    {
        $this->is_active = false;
        $this->save();
    }
}
