<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all()->where('is_active', true);
        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = new User($request->all());
        return view('users.create', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo $request->input('is_active');
        $validatedData = $request->validate([
            'login' => 'required|unique:users|max:255|min:3',
            'password_digest' => 'required|max:255|min:3',
            'firstname' => 'required|max:255|min:3',
            'lastname' => 'required|max:255|min:3',
            'birthdate' => 'required|max:255|min:3',
            'is_active' => 'nullable',
            'doc_image' => 'required|image',
        ]);

        if ($validatedData) {
            $file = $request->file('doc_image');

            $user = new User($validatedData);
            $user->is_active = boolval(isset($validatedData['is_active']) ? $validatedData['is_active'] : false);
            $user->doc_image = $file;

            $user->save();
            return redirect(route('users.show', [$user->id]));
        } else {
            $user = new User($request->all());
            return view('users.create', ['user' => $user]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
        $user->fill($request->all());
        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'login' => "required|unique:users,login,{$user->id}|max:255|min:3",
            'password_digest' => 'nullable|max:255|min:3',
            'firstname' => 'required|max:255|min:3',
            'lastname' => 'required|max:255|min:3',
            'birthdate' => 'required|max:255|min:3',
            'is_active' => 'nullable',
            'doc_image' => 'nullable|image',
        ]);

        if ($validatedData) {
            $file = $request->file('doc_image');

            $user->fill($validatedData);
            $user->is_active = boolval(isset($validatedData['is_active']) ? $validatedData['is_active'] : false);
            if ($file) {
                $user->doc_image = $file;
            }

            $user->save();
            return redirect(route('users.show', [$user->id]));
        } else {
            $user = new User($request->all());
            return view('users.create', ['user' => $user]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->softDelete();
        return redirect(route('users.index'));
    }
}
