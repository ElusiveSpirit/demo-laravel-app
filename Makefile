#!/usr/bin/make

SHELL = /bin/sh

up: ## Prepare and up the project
	docker-compose -f docker-compose.dev.yml run --rm laravel composer install --no-interaction --ansi --no-suggest
	docker-compose -f docker-compose.dev.yml run --rm laravel php artisan migrate
	docker-compose -f docker-compose.dev.yml run --rm laravel php artisan db:seed
	docker-compose -f docker-compose.dev.yml up

