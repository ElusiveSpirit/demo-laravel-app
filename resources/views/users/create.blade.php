@extends('layouts.app')

@section('content')
    <a href="/users">Назад</a>

    <h1>Создать пользователя</h1>

    <div class="text-left">

        @if ($errors->any())
            <div style="color: indianred;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => ['users.store'], 'files' => true, 'method' => 'post']) !!}
        {!! Form::token(); !!}

            @include('users.form')

        {!! Form::close() !!}
    </div>

@endsection
