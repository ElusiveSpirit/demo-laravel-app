@extends('layouts.app')

@section('content')
    <a href="{{ route('users.show', [$user->id]) }}">Назад</a>

    <h1>Изменить пользователя {{ $user->full_name }}</h1>
    <h1></h1>
    <div class="text-left">

        @if ($errors->any())
            <div style="color: indianred;">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route' => ['users.update', $user->id], 'files' => true, 'method' => 'patch']) !!}
        {!! Form::token(); !!}

            @include('users.form')

        {!! Form::close() !!}
    </div>

@endsection
