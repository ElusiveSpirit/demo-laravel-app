@extends('layouts.app')

@section('content')
    <a href="{{ route('users.index') }}">Назад</a>

    <h1>{{ $user->full_name }}</h1>

    <ul class="text-left">
        <li>Логин: {{ $user->login }}</li>
        <li>Пароль: {{ $user->password_digest }}</li>
        <li>Дата рождения: {{ $user->birthdate }}</li>
        <li>Активен?: {{ $user->is_active ? 'Да' : 'Нет' }}</li>
        <li>
            <div>
                Изображение документа:
            </div>
            <div>
                <img src="{{ $user->doc_image_url }}" alt="" style="max-width: 300%; max-height: 200px;">
            </div>
        </li>
    </ul>

    <a href="{{ route('users.edit', [$user->id]) }}">Изменить</a>

    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
        {!! Form::button('Удалить', ['type' => 'submit', 'onclick' => "return confirm('Вы уверены?')"]) !!}
    {!! Form::close() !!}
@endsection
