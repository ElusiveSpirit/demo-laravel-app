@extends('layouts.app')

@section('content')
    <h1>Пользователи</h1>

    <a href="{{ route('users.create') }}">Добавить пользователя</a>

    @foreach ($users as $user)
        <h3><a href="{{ route('users.show', [$user->id]) }}">{{ $user->full_name }}</a></h3>
        <p>{{ $user->login }}</p>
    @endforeach
@endsection
