<p class="form-group">
    {!! Form::label('login', 'Логин') !!}<br>
    {!! Form::text('login', $user->login, ['class' => 'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('password_digest', 'Пароль') !!}<br>
    {!! Form::password('password_digest', ['class' => 'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('firstname', 'Имя') !!}<br>
    {!! Form::text('firstname', $user->firstname, ['class' => 'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('lastname', 'Фамилия') !!}<br>
    {!! Form::text('lastname', $user->lastname, ['class' => 'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('birthdate', 'Дата рождения') !!}<br>
    {!! Form::date('birthdate', $user->birthdate, ['class' => 'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('is_active', 'Активный?') !!}<br>
    {!! Form::checkbox('is_active', 'is_active', $user->is_active) !!}
</p>
<p class="form-group">
    <div>Текущее изображение: {{ $user->doc_image }}</div>
    {!! Form::label('doc_image', 'Новое:') !!}</br>
    {!! Form::file('doc_image', null, ['class' => 'form-control']) !!}
</p>
<p>
{!! Form::submit('Сохранить', ['class' => 'btn btn-info']) !!}
</p>
