<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRetrieveClearUsersPage()
    {
        $response = $this->get('/users');

        $response->assertStatus(200)->assertSeeText('Пользователи');
    }
    public function testRetrieveUsersPageWithUser()
    {
        $user = factory(User::class)->create();
        $response = $this->get('/users');

        $response->assertStatus(200)
            ->assertSeeText('Пользователи')
            ->assertSeeText($user->full_name);
    }
    public function testRetrieveUserDetailPageWith()
    {
        $user = factory(User::class)->create();
        $response = $this->get("/users/{$user->id}");

        $response->assertStatus(200)
            ->assertSeeText($user->full_name)
            ->assertSeeText($user->login);
    }
}
